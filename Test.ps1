#Enter the SentinelOne site token here, within the quotes.
$SentinelSiteToken = "eyJ1cmwiOiAiaHR0cHM6Ly91c2VhMS1wYXg4LTAzLnNlbnRpbmVsb25lLm5ldCIsICJzaXRlX2tleSI6ICJiNWY2OGEzYjcyOTUxMmM0In0=";
#fill in a publicly available download URl for your SentinelOne *EXE* 64-bit installer here. 
$SentinelDownloadUrl = "https://cctideployment.s3.us-west-2.amazonaws.com/SentinelOneInstaller_windows_64bit_v22_1_4_10010.exe";

#this folder will be used or created, feel free to change it. This variable should not have a trailing \
$tempPath = "C:\temp\MSPTools";

#the script will save the SentinelOne installer as "S1.exe" in the above named folder
$SentinelPath = "$tempPath\S1.exe";

function TempPath
{
    if(Test-Path $tempPath)
    {
        Remove-Item "$tempPath\*";
    }
    else
    {
        New-Item $tempPath -ItemType Directory;
    }
}

function SentinelInstall
{
    If ((Get-WmiObject win32_operatingsystem | Select-Object osarchitecture).osarchitecture -eq "64-bit")
    {
        Set-Location $tempPath;
        (New-Object Net.WebClient).DownloadFile($SentinelDownloadUrl, $SentinelPath);
 #       Start-Process S1.exe -ArgumentList "/SILENT /SITE_TOKEN=$SentinelSiteToken"
    }
    else {
        Exit 1;
    }
}

TempPath;
SentinelInstall;

